package com.myhouse.company.adapter.pattern.client;

import com.myhouse.company.adapter.pattern.cart.IShoppingCart;
import com.myhouse.company.adapter.pattern.cart.ShoppingCart;
import com.myhouse.company.adapter.pattern.model.Product;
import com.myhouse.company.adapter.pattern.service.ShoppingCartService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;

public class FarmClientTest {

    private FarmClient farmClient;
    private IShoppingCart shoppingCart;
    private ShoppingCartService shoppingCartService;


    @Before
    public void setUp(){
        shoppingCartService = new ShoppingCartService();
        shoppingCart = new ShoppingCart(shoppingCartService);
        farmClient = new FarmClient(shoppingCart);
    }

    @Test
    public void can_calculate_total_price(){
        Product product = new Product();
        product.setWeight(2);
        product.setPrice(2.99);
        farmClient.addProduct(product);

        assertNotEquals(0.0d, farmClient.getPrice());
    }
}
