package com.myhouse.company.adapter.pattern.configuration;

import com.myhouse.company.adapter.pattern.factory.SimpleShoppingCartFactory;
import com.myhouse.company.adapter.pattern.model.Measures;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class FarmClientConfigurationTest {

    private FarmClientConfiguration farmClientConfiguration;
    private SimpleShoppingCartFactory simpleShoppingCartFactory;


    @Before
    public void setUp(){
        simpleShoppingCartFactory = new SimpleShoppingCartFactory();
        farmClientConfiguration = new FarmClientConfiguration(simpleShoppingCartFactory);
    }

    @Test
    public void can_set_measure(){
        farmClientConfiguration.setMeasure(Measures.KG);
    }

    @Test
    public void can_get_measure(){
        farmClientConfiguration.getMeasure();
    }

    @Test
    public void can_get_shopping_cart_configuration(){
        farmClientConfiguration.setMeasure(Measures.LBS);

        assertNotNull(farmClientConfiguration.getShoppingCart());
    }

    @Test
    public void can_get_shopping_cart_service_configuration(){
        farmClientConfiguration.setMeasure(Measures.LBS);

        assertNotNull(farmClientConfiguration.getShoppingCartService());
    }
}
