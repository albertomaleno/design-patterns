package com.myhouse.company.adapter.pattern.cart;

import com.myhouse.company.adapter.pattern.model.Product;
import com.myhouse.company.adapter.pattern.service.ShoppingCartService;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShoppingCartTest {

    private IShoppingCart shoppingCart;
    private ShoppingCartService shoppingCartService;
    private Product firstProduct, secondProduct;
    private List<Product> listProducts;

    @Before
    public void setUp(){
        shoppingCartService = new ShoppingCartService();
        shoppingCart = new ShoppingCart(shoppingCartService);
        firstProduct = new Product();
        firstProduct.setPrice(2);
        firstProduct.setWeight(3);
        secondProduct = new Product();
        secondProduct.setWeight(5);
        secondProduct.setPrice(5);
        listProducts = List.of(firstProduct, secondProduct);
        shoppingCart.addProduct(firstProduct);
        shoppingCart.addProduct(secondProduct);
    }

    @Test
    public void can_get_list_products(){
       assertEquals(listProducts, shoppingCart.getListProducts());
    }

    @Test
    public void can_get_total_price(){
        assertNotEquals(0.0d, shoppingCart.getTotalPrice());
    }


}
