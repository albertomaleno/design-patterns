package com.myhouse.company.adapter.pattern.cart;

import com.myhouse.company.adapter.pattern.model.Measures;
import com.myhouse.company.adapter.pattern.model.Product;
import com.myhouse.company.adapter.pattern.service.ShoppingCartService;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ShoppingCartPoundsAdapterTest {

    private static final double KG_TO_POUNDS = Measures.KG.getConversion();
    private IShoppingCart shoppingCartPoundsAdapter;
    private ShoppingCartService shoppingCartService;
    private Product firstProduct, secondProduct;
    private List<Product> listProducts;

    @Before
    public void setUp(){
        shoppingCartService = new ShoppingCartService();
        shoppingCartPoundsAdapter = new ShoppingCartPoundsAdapter(shoppingCartService);
        firstProduct = new Product();
        firstProduct.setPrice(2);
        firstProduct.setWeight(3);
        secondProduct = new Product();
        secondProduct.setWeight(5);
        secondProduct.setPrice(5);
        listProducts = List.of(firstProduct, secondProduct);
        shoppingCartPoundsAdapter.addProduct(firstProduct);
        shoppingCartPoundsAdapter.addProduct(secondProduct);
    }

    @Test
    public void can_get_list_products(){
        assertEquals(listProducts, shoppingCartPoundsAdapter.getListProducts());
    }

    @Test
    public void can_get_total_price_calculated_from_pounds(){
        final double expectedPrice = ((firstProduct.getWeight() * KG_TO_POUNDS) * firstProduct.getPrice())
                + ((secondProduct.getWeight() * KG_TO_POUNDS) * secondProduct.getPrice());

        assertEquals(expectedPrice, shoppingCartPoundsAdapter.getTotalPrice(), 0.0d);
    }
}
