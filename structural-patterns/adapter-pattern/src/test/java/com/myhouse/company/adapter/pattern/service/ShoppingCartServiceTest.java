package com.myhouse.company.adapter.pattern.service;

import com.myhouse.company.adapter.pattern.model.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShoppingCartServiceTest {

    private ShoppingCartService shoppingCartService;


    @Before
    public void setUp(){
        shoppingCartService = new ShoppingCartService();
    }

    @Test
    public void can_calculate_products_price(){
        final double expectedPrice = 3;
        Product product = new Product();
        product.setPrice(1.5);
        product.setWeight(2);
        List<Product> listProducts = List.of(product);

        Assert.assertEquals(expectedPrice, shoppingCartService.calculatePrice(listProducts), 0);
    }
}
