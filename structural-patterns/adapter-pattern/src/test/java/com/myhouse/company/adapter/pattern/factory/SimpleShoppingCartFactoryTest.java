package com.myhouse.company.adapter.pattern.factory;

import com.myhouse.company.adapter.pattern.cart.ShoppingCart;
import com.myhouse.company.adapter.pattern.cart.ShoppingCartPoundsAdapter;
import com.myhouse.company.adapter.pattern.configuration.FarmClientConfiguration;
import com.myhouse.company.adapter.pattern.model.Measures;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SimpleShoppingCartFactoryTest {

    private SimpleShoppingCartFactory simpleShoppingCartFactory;
    private FarmClientConfiguration farmClientConfiguration;

    @Before
    public void setUp(){
        simpleShoppingCartFactory = new SimpleShoppingCartFactory();
        farmClientConfiguration = new FarmClientConfiguration(simpleShoppingCartFactory);
    }

    @Test
    public void measure_set_to_kg_returns_shopping_cart(){
        farmClientConfiguration.setMeasure(Measures.KG);

        assertTrue(simpleShoppingCartFactory.getShoppingCart(farmClientConfiguration) instanceof ShoppingCart);
    }

    @Test
    public void measure_set_to_lbs_returns_shopping_cart_pounds_adapter(){
        farmClientConfiguration.setMeasure(Measures.LBS);

        assertTrue(simpleShoppingCartFactory.getShoppingCart(farmClientConfiguration) instanceof ShoppingCartPoundsAdapter);
    }
}
