package com.myhouse.company.adapter.pattern;

import com.myhouse.company.adapter.pattern.cart.IShoppingCart;
import com.myhouse.company.adapter.pattern.configuration.FarmClientConfiguration;
import com.myhouse.company.adapter.pattern.factory.SimpleShoppingCartFactory;
import com.myhouse.company.adapter.pattern.model.Measures;
import com.myhouse.company.adapter.pattern.model.Product;

public class FarmApplication {

    public static void main(String[] args){
        FarmClientConfiguration farmClientConfiguration = new FarmClientConfiguration(new SimpleShoppingCartFactory());
        farmClientConfiguration.setMeasure(Measures.LBS);
        IShoppingCart shoppingCart = farmClientConfiguration.getShoppingCart();

        // Lets add some tomatoes!!
        Product tomatoes = new Product();
        tomatoes.setPrice(0.4);
        tomatoes.setWeight(4);

        shoppingCart.addProduct(tomatoes);

        shoppingCart.getTotalPrice();
    }
}
