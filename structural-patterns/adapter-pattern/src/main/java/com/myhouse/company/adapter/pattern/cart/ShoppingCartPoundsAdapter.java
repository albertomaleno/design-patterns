package com.myhouse.company.adapter.pattern.cart;

import com.myhouse.company.adapter.pattern.model.Measures;
import com.myhouse.company.adapter.pattern.model.Product;
import com.myhouse.company.adapter.pattern.service.ShoppingCartService;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartPoundsAdapter implements IShoppingCart {

    private static final double KG_TO_POUND = Measures.KG.getConversion();
    private final ShoppingCartService shoppingCartService;
    private final List<Product> listProducts = new ArrayList<>();



    public ShoppingCartPoundsAdapter(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    @Override
    public void addProduct(Product product) {
        System.out.println("Adding a product: " + product.toString());
        listProducts.add(product);
    }

    @Override
    public List<Product> getListProducts() {
        return listProducts;
    }

    @Override
    public double getTotalPrice() {
        for (Product product: listProducts){
            double pounds = product.getWeight() * KG_TO_POUND;
            product.setWeight(pounds);
        }
        return shoppingCartService.calculatePrice(listProducts);
    }
}
