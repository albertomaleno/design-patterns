package com.myhouse.company.adapter.pattern.model;

public enum Measures {

    KG(2.204623d),
    LBS(0.4535924d);

    private final double conversion;

    Measures(double fromTo) {
        this.conversion = fromTo;
    }

    public double getConversion() {
        return conversion;
    }
}
