package com.myhouse.company.adapter.pattern.cart;

import com.myhouse.company.adapter.pattern.model.Product;

import java.util.List;

public interface IShoppingCart {

    /**
     * Adds a product to the shopping cart
     *
     * @param product
     */
    void addProduct(Product product);

    /**
     * Gets the current list of products
     * @return listProducts
     */
    List<Product> getListProducts();

    /**
     * Calculates the total price of the products inside the shopping cart
     *
     * @return totalPrice
     */
    double getTotalPrice();
}
