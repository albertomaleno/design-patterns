package com.myhouse.company.adapter.pattern.factory;

import com.myhouse.company.adapter.pattern.cart.IShoppingCart;
import com.myhouse.company.adapter.pattern.cart.ShoppingCart;
import com.myhouse.company.adapter.pattern.cart.ShoppingCartPoundsAdapter;
import com.myhouse.company.adapter.pattern.configuration.FarmClientConfiguration;
import com.myhouse.company.adapter.pattern.model.Measures;

public class SimpleShoppingCartFactory {

    public IShoppingCart getShoppingCart(FarmClientConfiguration farmClientConfiguration){
        if (farmClientConfiguration.getMeasure().equals(Measures.KG)){
            return new ShoppingCart(farmClientConfiguration.getShoppingCartService());
        }else{
            return new ShoppingCartPoundsAdapter(farmClientConfiguration.getShoppingCartService());
        }
    }
}
