package com.myhouse.company.adapter.pattern.cart;

import com.myhouse.company.adapter.pattern.model.Product;
import com.myhouse.company.adapter.pattern.service.ShoppingCartService;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements IShoppingCart{

    private final List<Product> listProducts = new ArrayList<>();
    private final ShoppingCartService shoppingCartService;

    public ShoppingCart(ShoppingCartService shoppingCartService){
        this.shoppingCartService = shoppingCartService;
    }

    public void addProduct(Product product) {
        System.out.println("Adding a product: " + product.toString());
        listProducts.add(product);
    }

    public List<Product> getListProducts() {
        return listProducts;
    }

    public double getTotalPrice() {
        return shoppingCartService.calculatePrice(listProducts);
    }
}
