package com.myhouse.company.adapter.pattern.client;

import com.myhouse.company.adapter.pattern.cart.IShoppingCart;
import com.myhouse.company.adapter.pattern.model.Product;

public class FarmClient {

    private final IShoppingCart shoppingCart;

    public FarmClient(IShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void addProduct(Product product) {
        shoppingCart.addProduct(product);
    }

    public double getPrice() {
        return shoppingCart.getTotalPrice();
    }
}
