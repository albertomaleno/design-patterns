package com.myhouse.company.adapter.pattern.service;

import com.myhouse.company.adapter.pattern.model.Product;

import java.util.List;

public class ShoppingCartService {


    public double calculatePrice(List<Product> listProducts) {
        double totalPrice = 0.0d;
        for (Product product: listProducts){
            totalPrice += product.getPrice() * product.getWeight();
        }
        System.out.println("Total price: " + totalPrice);
        return totalPrice;
    }
}
