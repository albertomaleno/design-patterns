package com.myhouse.company.adapter.pattern.configuration;

import com.myhouse.company.adapter.pattern.cart.IShoppingCart;
import com.myhouse.company.adapter.pattern.factory.SimpleShoppingCartFactory;
import com.myhouse.company.adapter.pattern.model.Measures;
import com.myhouse.company.adapter.pattern.service.ShoppingCartService;

public class FarmClientConfiguration {

    private final SimpleShoppingCartFactory simpleShoppingCartFactory;
    private Measures measure;

    public FarmClientConfiguration(SimpleShoppingCartFactory simpleShoppingCartFactory) {
        this.simpleShoppingCartFactory = simpleShoppingCartFactory;
    }

    public void setMeasure(Measures measure) {
        this.measure = measure;
    }

    public Measures getMeasure(){
        return measure;
    }

    public IShoppingCart getShoppingCart() {
        return simpleShoppingCartFactory.getShoppingCart(this);
    }

    public ShoppingCartService getShoppingCartService(){
        return new ShoppingCartService();
    }
}
