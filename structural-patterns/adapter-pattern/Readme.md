# Adapter-Pattern



Structural pattern that allows two incompatible interfaces to collaborate between themselves by providing an intermediary who translates the calls from one interface to another. 



## Application introduction



A company sells products from a farm by weight, being all of them specified in kilograms, but many new people have recently moved abroad to the town, being most of them from United states, where they usually buy their products in pounds. The old application they use to calculate the total price is not prepared to translate the total price of the things the customers buy from kilograms to pounds, but they want to offer this possibility.



![image-20201230213054870](/home/alberto/Git/design-patterns/structural-patterns/adapter-pattern/uml.png)