# Design Patterns



[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)



This repository is a practice being done while reading the Refactoring Guru's design patterns book.

For every pattern, there is an application designed from scratch in which the pattern that is being learned is applied.