# Prototype-Pattern



Creational pattern that provides a way to decouple the task of cloning objects from the client code. It relegates the responsibility of cloning into the objects to be copied themselves, leaving them the responsibility of deciding which fields are to be copied. It also solves the encapsulation problem since many fields of an object can have private access.





## Application introduction



Every cell has a process called mitosis by which it replicates itself into two new nuclei. As only the cell itself knows what information it contains, no other cell can find the information about others to copy them. There is also another problem, a scientist wants to know what cells a body contains and start working with them, therefore, he needs some kind of way to manage this. He/She also has discovered how to command cells to clone themselves and he will probably use it to investigate.





![image-20200925151952838](/home/alberto/Git/design-patterns/prototype-pattern/uml.png)