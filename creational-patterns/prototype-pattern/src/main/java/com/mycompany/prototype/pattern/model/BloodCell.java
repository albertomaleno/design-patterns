package com.mycompany.prototype.pattern.model;

public class BloodCell extends PrototypeCell {

    public BloodCell() {
    }

    public BloodCell(BloodCell bloodCell) {
        super(bloodCell);
    }


    @Override
    public String getName() {
        return "BloodCell";
    }

    @Override
    public String getMembrane() {
        return "Membrane";
    }

    @Override
    public String getCytoSkeleton() {
        return "CytoSkeleton";
    }

    @Override
    public String getGeneticMaterial() {
        return "ABCACB";
    }

    @Override
    public PrototypeCell clone() {
        return new BloodCell(this);
    }
}
