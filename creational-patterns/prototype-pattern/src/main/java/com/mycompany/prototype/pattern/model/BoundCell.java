package com.mycompany.prototype.pattern.model;

public class BoundCell extends PrototypeCell {

    public BoundCell() {
    }

    public BoundCell(BoundCell bloodCell) {
        super(bloodCell);
    }


    @Override
    public String getName() {
        return "BoundCell";
    }

    @Override
    public String getMembrane() {
        return "Membrane";
    }

    @Override
    public String getCytoSkeleton() {
        return "CytoSkeleton";
    }

    @Override
    public String getGeneticMaterial() {
        return "ABCACB";
    }

    @Override
    public PrototypeCell clone() {
        return new BoundCell(this);
    }
}
