package com.mycompany.prototype.pattern.registry;

import com.mycompany.prototype.pattern.model.PrototypeCell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CellRegistry {


    private Map<String, PrototypeCell> mapRegistry = new HashMap<>();

    public void addCell(PrototypeCell cell){
        mapRegistry.put(cell.getName(), cell);
    }

    public PrototypeCell getCell(String name){
        return mapRegistry.get(name);
    }

    public void removeCell(String name){
        mapRegistry.remove(name);
    }

    public List<PrototypeCell> cloneCells(){
        List<PrototypeCell> listClonedCells = new ArrayList<>();
        for (PrototypeCell prototypeCell: mapRegistry.values()){
            listClonedCells.add(prototypeCell.clone());
        }
        return listClonedCells;
    }

}
