package com.mycompany.prototype.pattern.model;

public abstract class PrototypeCell {

    private String name;
    private String membrane;
    private String cytoSkeleton;
    private String geneticMaterial;

    public PrototypeCell(){}

    protected PrototypeCell(PrototypeCell prototypeCell){
        this.name = prototypeCell.name;
        this.membrane = prototypeCell.membrane;
        this.cytoSkeleton = prototypeCell.cytoSkeleton;
        this.geneticMaterial = prototypeCell.geneticMaterial;
    }


    /**
    * Gets the name of the cell
    * @return cellName
    */
    public abstract String getName();


    /**
    * Gets the cell membrane
    * @return membrane
    */
    public abstract String getMembrane();

    /**
    * Gets the cell cytoSkeleton
    * @return cytoSkeleton
    */
    public abstract String getCytoSkeleton();

    /**
    * Gets the cell genetic material
    * @return geneticMaterial
    */
    public abstract String getGeneticMaterial();


    /**
     * Clones the cell itself with all its fields
     * @return a nw prototypeCell instance
     */
    public abstract PrototypeCell clone();
}
