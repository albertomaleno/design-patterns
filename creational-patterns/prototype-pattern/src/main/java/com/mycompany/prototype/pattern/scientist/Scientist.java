package com.mycompany.prototype.pattern.scientist;

import com.mycompany.prototype.pattern.model.PrototypeCell;
import com.mycompany.prototype.pattern.registry.CellRegistry;

import java.util.List;

public class Scientist {

    private CellRegistry cellRegistry;

    public Scientist(CellRegistry cellRegistry) {
        this.cellRegistry = cellRegistry;
    }

    public void checkCell(String name){
        System.out.println("Checking cell...");
        PrototypeCell prototypeCell = cellRegistry.getCell(name);
        System.out.println("Wow, amazing generic material that has this " + prototypeCell.getName() + " inside");
    }

    public void registerNewCell(PrototypeCell cell){
        System.out.println("Wow, what a discovery, let's register this new cell");
        cellRegistry.addCell(cell);
    }

    public void removeCell(String name){
        System.out.println("mmm, i'm not interested anymore in " + name + " cells, let's remove it from the investigation registry");
        cellRegistry.removeCell(name);
    }

    public void commandCellClone(){
        System.out.println("I think i can clone cells....");
        List<PrototypeCell> prototypeCellList = cellRegistry.cloneCells();

        for (PrototypeCell prototypeCell: prototypeCellList){
            System.out.println("Wow, i have commanded a " + prototypeCell.getName() + " cell to clone itself, yuhu!");
        }

        System.out.println("Mm, now i can try inverting the process so i can fix the cancer :)");
    }
}
