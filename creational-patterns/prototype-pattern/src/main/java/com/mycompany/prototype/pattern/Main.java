package com.mycompany.prototype.pattern;

import com.mycompany.prototype.pattern.model.BloodCell;
import com.mycompany.prototype.pattern.model.BoundCell;
import com.mycompany.prototype.pattern.model.PrototypeCell;
import com.mycompany.prototype.pattern.registry.CellRegistry;
import com.mycompany.prototype.pattern.scientist.Scientist;

public class Main {

    public static void main(String[] args){
        CellRegistry cellRegistry = new CellRegistry();
        BloodCell bloodCell = new BloodCell();
        cellRegistry.addCell(bloodCell);
        Scientist scientist = new Scientist(cellRegistry);

        scientist.checkCell(bloodCell.getName());
        scientist.registerNewCell(new BoundCell());
        scientist.commandCellClone();
        scientist.removeCell(bloodCell.getName());


    }
}
