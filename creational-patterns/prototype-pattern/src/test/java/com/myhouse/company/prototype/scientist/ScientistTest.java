package com.myhouse.company.prototype.scientist;

import com.mycompany.prototype.pattern.model.BloodCell;
import com.mycompany.prototype.pattern.model.BoundCell;
import com.mycompany.prototype.pattern.registry.CellRegistry;
import com.mycompany.prototype.pattern.scientist.Scientist;
import org.junit.Before;
import org.junit.Test;

public class ScientistTest {

    private static String firstCellName;
    private static String secondCellName;
    private CellRegistry cellRegistry;
    private Scientist scientist;


    @Before
    public void setUp(){
        BloodCell bloodCell = new BloodCell();
        BoundCell boundCell = new BoundCell();
        cellRegistry = new CellRegistry();
        cellRegistry.addCell(bloodCell);
        cellRegistry.addCell(boundCell);

        firstCellName = bloodCell.getName();
        secondCellName = boundCell.getName();
    }

    @Test
    public void when_CheckCellIsInvoked_Expect_ScientistCanCheckCell(){
        scientist = new Scientist(cellRegistry);

        scientist.checkCell(firstCellName);
    }

    @Test
    public void when_RegisterNewCellIsInvoked_Expect_ScientistCanRegisterNewCell(){
        scientist = new Scientist(cellRegistry);

        scientist.registerNewCell(new BloodCell());
    }

    @Test
    public void when_RemoveCellIsInvoked_Expect_ScientistCanRemoveCell(){
        scientist = new Scientist(cellRegistry);

        scientist.removeCell(firstCellName);
    }


    @Test
    public void when_CommandCellCloneIsInvoked_Expect_ScientistCanInvestigateTheClonedCells(){
        scientist = new Scientist(cellRegistry);

        scientist.commandCellClone();
    }
}
