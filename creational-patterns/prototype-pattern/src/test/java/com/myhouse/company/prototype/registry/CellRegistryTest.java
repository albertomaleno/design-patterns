package com.myhouse.company.prototype.registry;

import static org.junit.Assert.*;

import com.mycompany.prototype.pattern.model.BloodCell;
import com.mycompany.prototype.pattern.model.BoundCell;
import com.mycompany.prototype.pattern.model.PrototypeCell;
import com.mycompany.prototype.pattern.registry.CellRegistry;
import org.junit.Test;

import java.util.List;

public class CellRegistryTest {

    private CellRegistry cellRegistry;

    @Test
    public void when_CellIsAdded_Expect_CellToBeRetrieved(){
        cellRegistry = new CellRegistry();
        BloodCell bloodCell = new BloodCell();

        cellRegistry.addCell(bloodCell);

        assertEquals(bloodCell, cellRegistry.getCell(bloodCell.getName()));
    }

    @Test
    public void when_CellIsRemoved_Expect_CellNotRetrievable(){
        cellRegistry = new CellRegistry();
        BloodCell bloodCell = new BloodCell();

        cellRegistry.addCell(bloodCell);
        cellRegistry.removeCell(bloodCell.getName());

        assertNull(cellRegistry.getCell(bloodCell.getName()));
    }

    @Test
    public void when_CloneIsInvoked_Expect_AllCellsCloned(){
        cellRegistry = new CellRegistry();
        BloodCell bloodCell = new BloodCell();
        BoundCell boundCell = new BoundCell();
        cellRegistry.addCell(bloodCell);
        cellRegistry.addCell(boundCell);

        List<PrototypeCell> listClonedCells = cellRegistry.cloneCells();

        assertEquals(2, listClonedCells.size());
    }


}
