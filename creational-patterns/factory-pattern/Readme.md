# Factory-Pattern





Creational design pattern that provides an interface for creating  objects in a superclass, but allows subclasses to alter the type of  objects that will be created.





### Application introduction



An athlete wants to prepare its training for a competition in which he/she needs to practice different sports in short time periods. To be prepared for the competition, he/she, needs to train a lot every day of the week and, therefore, needs to practice every sport for the competition. To be ready, he/she has taken a decision, every day he/she will be practicing a new sport, hence, it depends on the current day of the week to know which sport will do today.





### Application design





![image-20200804170445792](/home/alberto/.config/Typora/typora-user-images/image-20200804170445792.png)