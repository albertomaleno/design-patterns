package com.myhouse.factory.pattern.factory;

import org.junit.Test;

import java.time.DayOfWeek;

import static org.junit.Assert.assertTrue;

public class TrainingProviderTest {

    private TrainingProvider factoryProvider;

    @Test
    public void when_DayOfWeekIsMondayOrThursday_Expect_SwimTrainingFactory(){
        factoryProvider = new TrainingProvider();
        DayOfWeek monday = DayOfWeek.MONDAY;
        DayOfWeek thursday = DayOfWeek.THURSDAY;

        assertTrue(factoryProvider.getTodayTraining(monday) instanceof SwimTraining);
        assertTrue(factoryProvider.getTodayTraining(thursday) instanceof SwimTraining);
    }

    @Test
    public void when_DayOfWeekIsTuesdayOrFriday_Expect_RunTrainingFactory(){
        factoryProvider = new TrainingProvider();
        DayOfWeek tuesday = DayOfWeek.TUESDAY;
        DayOfWeek friday = DayOfWeek.FRIDAY;

        assertTrue(factoryProvider.getTodayTraining(tuesday) instanceof RunTraining);
        assertTrue(factoryProvider.getTodayTraining(friday) instanceof RunTraining);
    }


    @Test
    public void when_DayOfWeekIsWednesdaySaturdayOrSunday_Expect_CycleTrainingFactory(){
        factoryProvider = new TrainingProvider();
        DayOfWeek wednesday = DayOfWeek.WEDNESDAY;
        DayOfWeek saturday = DayOfWeek.SATURDAY;
        DayOfWeek sunday = DayOfWeek.SUNDAY;

        assertTrue(factoryProvider.getTodayTraining(wednesday) instanceof CycleTraining);
        assertTrue(factoryProvider.getTodayTraining(saturday) instanceof CycleTraining);
        assertTrue(factoryProvider.getTodayTraining(sunday) instanceof CycleTraining);
    }
}
