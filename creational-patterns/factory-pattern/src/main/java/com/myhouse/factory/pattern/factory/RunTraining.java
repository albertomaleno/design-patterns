package com.myhouse.factory.pattern.factory;

import com.myhouse.factory.pattern.traning.Running;
import com.myhouse.factory.pattern.traning.Training;

public class RunTraining extends TodayTraining {

    @Override
    public Training getTraining() {
        return new Running();
    }
}
