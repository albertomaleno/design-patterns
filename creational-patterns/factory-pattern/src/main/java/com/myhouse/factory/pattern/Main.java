package com.myhouse.factory.pattern;

import com.myhouse.factory.pattern.factory.*;
import com.myhouse.factory.pattern.model.Athlete;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args){
        TrainingProvider factoryProvider = new TrainingProvider();
        TodayTraining todayTraining = factoryProvider.getTodayTraining(LocalDate.now().getDayOfWeek());
        Athlete athlete = new Athlete();
        athlete.setName("Iker");
        todayTraining.train(athlete);
    }
}
