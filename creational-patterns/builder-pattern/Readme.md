# Builder-Pattern



Creational pattern that provides a way to construct a family of related products step by step, encapsulating the logic of every of them into objects called builders. Every of these objects follow an interface that groups the necessary steps to produce the products.



### Application introduction

A factory has an application to create multiple kinds of airplanes and they want to group the common steps into the same place so they can reuse them. At the moment they have two types of airplanes:

- Commercial commercialAirplane
- Private Jet commercialAirplane

These two airplanes are compound by:

-  A number of Seats
- A number of wheels
- Precooked food or cooked food by a chef



![image-20200922211043721](/home/alberto/.config/Typora/typora-user-images/image-20200922211043721.png)