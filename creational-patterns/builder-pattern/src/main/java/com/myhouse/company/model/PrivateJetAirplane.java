package com.myhouse.company.model;

public class PrivateJetAirplane {

    private final ChampagneBottle champagneBottle = new ChampagneBottle();
    private int numberOfSeats;
    private int numberOfWheels;
    private FoodType foodType;

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void setNumberOfWheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public ChampagneBottle getChampagne() {
        return champagneBottle;
    }

    @Override
    public String toString() {
        return "PrivateJetAirplane{" +
                "champagneBottle=" + champagneBottle +
                ", numberOfSeats=" + numberOfSeats +
                ", numberOfWheels=" + numberOfWheels +
                ", foodType=" + foodType +
                '}';
    }
}
