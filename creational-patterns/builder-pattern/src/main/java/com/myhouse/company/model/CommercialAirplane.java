package com.myhouse.company.model;

public class CommercialAirplane {

    private int numberOfSeats;
    private int numberOfWheels;
    private FoodType foodType;

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void setNumberOfWheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    @Override
    public String toString() {
        return "CommercialAirplane{" +
                "numberOfSeats=" + numberOfSeats +
                ", numberOfWheels=" + numberOfWheels +
                ", foodType=" + foodType +
                '}';
    }
}
