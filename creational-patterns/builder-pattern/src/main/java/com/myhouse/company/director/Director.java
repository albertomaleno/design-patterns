package com.myhouse.company.director;

import com.myhouse.company.builder.CommercialAirplaneBuilder;
import com.myhouse.company.builder.PrivateJetAirplaneBuilder;
import com.myhouse.company.model.ChefFood;
import com.myhouse.company.model.CommercialAirplane;
import com.myhouse.company.model.PrecookedFood;
import com.myhouse.company.model.PrivateJetAirplane;

public class Director {

   public CommercialAirplane createCommercialAirplane(CommercialAirplaneBuilder commercialAirplaneBuilder){
       commercialAirplaneBuilder.setNumberOfWheels(16);
       commercialAirplaneBuilder.setFoodType(new PrecookedFood());
       commercialAirplaneBuilder.setNumberOfSeats(100);

       CommercialAirplane commercialAirplane = commercialAirplaneBuilder.build();
       commercialAirplaneBuilder.reset();
       return commercialAirplane;
   }

    public PrivateJetAirplane createPrivateJetAirplane(PrivateJetAirplaneBuilder privateJetAirplaneBuilder){
        privateJetAirplaneBuilder.setNumberOfWheels(8);
        privateJetAirplaneBuilder.setFoodType(new ChefFood());
        privateJetAirplaneBuilder.setNumberOfSeats(10);

        PrivateJetAirplane privateJetAirplane = privateJetAirplaneBuilder.build();
        privateJetAirplaneBuilder.reset();
        return privateJetAirplane;
    }
}
