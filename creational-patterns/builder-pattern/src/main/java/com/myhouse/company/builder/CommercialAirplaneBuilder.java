package com.myhouse.company.builder;

import com.myhouse.company.model.CommercialAirplane;
import com.myhouse.company.model.FoodType;

public class CommercialAirplaneBuilder implements AirplaneBuilder {

    private CommercialAirplane commercialAirplane = new CommercialAirplane();

    @Override
    public void reset() {
        commercialAirplane = new CommercialAirplane();
    }

    @Override
    public void setNumberOfSeats(int numberOfSeats) {
        commercialAirplane.setNumberOfSeats(numberOfSeats);
    }

    @Override
    public void setNumberOfWheels(int numberOfWheels) {
        commercialAirplane.setNumberOfWheels(numberOfWheels);
    }

    @Override
    public void setFoodType(FoodType foodType) {
        commercialAirplane.setFoodType(foodType);
    }

    public CommercialAirplane build(){
        return commercialAirplane;
    }
}
