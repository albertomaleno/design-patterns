package com.myhouse.company.builder;

import com.myhouse.company.model.FoodType;

public interface AirplaneBuilder {

    void reset();

    void setNumberOfSeats(int numberOfSeats);

    void setNumberOfWheels(int numberOfWheels);

    void setFoodType(FoodType foodType);

}
