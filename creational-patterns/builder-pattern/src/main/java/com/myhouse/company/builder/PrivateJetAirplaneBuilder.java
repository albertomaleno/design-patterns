package com.myhouse.company.builder;

import com.myhouse.company.model.FoodType;
import com.myhouse.company.model.PrivateJetAirplane;

public class PrivateJetAirplaneBuilder implements AirplaneBuilder {

    private PrivateJetAirplane privateJetAirplane = new PrivateJetAirplane();

    @Override
    public void reset() {
        privateJetAirplane = new PrivateJetAirplane();
    }

    @Override
    public void setNumberOfSeats(int numberOfSeats) {
        privateJetAirplane.setNumberOfSeats(numberOfSeats);
    }

    @Override
    public void setNumberOfWheels(int numberOfWheels) {
        privateJetAirplane.setNumberOfWheels(numberOfWheels);
    }

    @Override
    public void setFoodType(FoodType foodType) {
        privateJetAirplane.setFoodType(foodType);
    }

    public PrivateJetAirplane build(){
        return privateJetAirplane;
    }
}
