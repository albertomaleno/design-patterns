package com.myhouse.company.model;

public interface FoodType {

    String getFood();
}
