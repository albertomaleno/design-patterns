package com.myhouse.company;

import com.myhouse.company.builder.CommercialAirplaneBuilder;
import com.myhouse.company.builder.PrivateJetAirplaneBuilder;
import com.myhouse.company.director.Director;
import com.myhouse.company.model.CommercialAirplane;
import com.myhouse.company.model.PrivateJetAirplane;

public class Main {

    public static void main(String[] args){
        Director director = new Director();

        CommercialAirplane commercialAirplane = director.createCommercialAirplane(new CommercialAirplaneBuilder());
        PrivateJetAirplane privateJetAirplane = director.createPrivateJetAirplane(new PrivateJetAirplaneBuilder());

        System.out.println("We have created a new commercial airplane! " + commercialAirplane.toString());
        System.out.println("We have created a new private jet airplane! " + privateJetAirplane.toString());
    }
}
