package com.myhouse.company.model;

public class ChampagneBottle {

    private double capacity = 1;

    public void drink(){
        System.out.println("Drinking champagne...");
        capacity -= 0.1;
        System.out.println("Champagne left: " + capacity + "L");
    }
}
