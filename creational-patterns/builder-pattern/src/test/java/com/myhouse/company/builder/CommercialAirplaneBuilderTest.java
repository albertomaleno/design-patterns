package com.myhouse.company.builder;

import static org.junit.Assert.*;

import com.myhouse.company.model.CommercialAirplane;
import com.myhouse.company.model.FoodType;
import com.myhouse.company.model.PrecookedFood;
import org.junit.Test;

public class CommercialAirplaneBuilderTest {

    private CommercialAirplaneBuilder commercialAirplaneBuilder;

    @Test
    public void when_buildIsInvoked_Expect_Airplane(){
        commercialAirplaneBuilder = new CommercialAirplaneBuilder();

        CommercialAirplane commercialAirplane = commercialAirplaneBuilder.build();
    }

    @Test
    public void when_setSeatsIsInvoked_Expect_CommercialAirplaneWithSeats(){
        commercialAirplaneBuilder = new CommercialAirplaneBuilder();

        final int numberOfSeats = 1;
        commercialAirplaneBuilder.setNumberOfSeats(numberOfSeats);
        CommercialAirplane commercialAirplane = commercialAirplaneBuilder.build();

        assertEquals(numberOfSeats, commercialAirplane.getNumberOfSeats());
    }

    @Test
    public void when_setWheelsIsInvoked_Expect_CommercialAirplaneWithWheels(){
        commercialAirplaneBuilder = new CommercialAirplaneBuilder();

        final int numberOfWheels = 2;
        commercialAirplaneBuilder.setNumberOfWheels(numberOfWheels);
        CommercialAirplane commercialAirplane = commercialAirplaneBuilder.build();

        assertEquals(numberOfWheels, commercialAirplane.getNumberOfWheels());
    }

    @Test
    public void when_setFoodTypeIsInvoked_Expect_CommercialAirplaneWithFoodType(){
        commercialAirplaneBuilder = new CommercialAirplaneBuilder();

        final FoodType foodType = new PrecookedFood();
        commercialAirplaneBuilder.setFoodType(foodType);
        CommercialAirplane commercialAirplane = commercialAirplaneBuilder.build();

        assertEquals(foodType, commercialAirplane.getFoodType());
    }

    @Test
    public void when_resetIsInvoked_Expect_NewCommercialAirplaneInstance(){
        commercialAirplaneBuilder = new CommercialAirplaneBuilder();

        final int numberOfWheels = 40;
        commercialAirplaneBuilder.setNumberOfWheels(numberOfWheels);
        commercialAirplaneBuilder.reset();
        CommercialAirplane commercialAirplane = commercialAirplaneBuilder.build();

        assertNotEquals(numberOfWheels, commercialAirplane.getNumberOfWheels());
    }
}
