package com.myhouse.company.director;

import static org.junit.Assert.*;

import com.myhouse.company.builder.CommercialAirplaneBuilder;
import com.myhouse.company.builder.PrivateJetAirplaneBuilder;
import com.myhouse.company.model.CommercialAirplane;
import com.myhouse.company.model.PrivateJetAirplane;
import org.junit.Test;

public class DirectorTest {

    private Director director;

    @Test
    public void when_CreateCommercialAirplaneIsInvoked_Expect_CommercialAirplane(){
      director = new Director();

      CommercialAirplane commercialAirplane = director.createCommercialAirplane(new CommercialAirplaneBuilder());
    }

    @Test
    public void when_CreatePrivateJetAirplaneIsInvoked_Expect_CommercialAirplane(){
        director = new Director();

        PrivateJetAirplane privateJetAirplane = director.createPrivateJetAirplane(new PrivateJetAirplaneBuilder());
    }

    @Test
    public void when_CreateCommercialAirplaneIsInvokedMultipleTimes_Expect_DifferentCommercialAirplaneInstances(){
        director = new Director();
        CommercialAirplaneBuilder commercialAirplaneBuilder = new CommercialAirplaneBuilder();

        CommercialAirplane commercialAirplane = director.createCommercialAirplane(commercialAirplaneBuilder);
        CommercialAirplane secondCommercialAirplane = director.createCommercialAirplane(commercialAirplaneBuilder);

        assertNotEquals(commercialAirplane, secondCommercialAirplane);
    }

    @Test
    public void when_CreatePrivateJetAirplaneIsInvokedMultipleTimes_Expect_DifferentPrivateJetAirplaneInstances(){
        director = new Director();
        PrivateJetAirplaneBuilder privateJetAirplaneBuilder = new PrivateJetAirplaneBuilder();

        PrivateJetAirplane privateJetAirplane = director.createPrivateJetAirplane(privateJetAirplaneBuilder);
        PrivateJetAirplane secondPrivateJetAirplane = director.createPrivateJetAirplane(privateJetAirplaneBuilder);

        assertNotEquals(privateJetAirplane, secondPrivateJetAirplane);
    }
}
