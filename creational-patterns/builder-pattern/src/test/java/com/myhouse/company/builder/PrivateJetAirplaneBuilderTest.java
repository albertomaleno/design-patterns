package com.myhouse.company.builder;

import com.myhouse.company.model.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrivateJetAirplaneBuilderTest {

    private PrivateJetAirplaneBuilder privateJetAirplaneBuilder;

    @Test
    public void when_buildIsInvoked_Expect_PrivateJetAirplane(){
        privateJetAirplaneBuilder = new PrivateJetAirplaneBuilder();

        PrivateJetAirplane privateAirplane = privateJetAirplaneBuilder.build();
    }

    @Test
    public void when_setSeatsIsInvoked_Expect_PrivateJetAirplaneWithSeats(){
        privateJetAirplaneBuilder = new PrivateJetAirplaneBuilder();

        final int numberOfSeats = 1;
        privateJetAirplaneBuilder.setNumberOfSeats(numberOfSeats);
        PrivateJetAirplane privateAirplane = privateJetAirplaneBuilder.build();

        assertEquals(numberOfSeats, privateAirplane.getNumberOfSeats());
    }

    @Test
    public void when_setWheelsIsInvoked_Expect_PrivateJetAirplaneWithWheels(){
        privateJetAirplaneBuilder = new PrivateJetAirplaneBuilder();

        final int numberOfWheels = 2;
        privateJetAirplaneBuilder.setNumberOfWheels(numberOfWheels);
        PrivateJetAirplane privateAirplane = privateJetAirplaneBuilder.build();

        assertEquals(numberOfWheels, privateAirplane.getNumberOfWheels());
    }

    @Test
    public void when_setFoodTypeIsInvoked_Expect_PrivateJetAirplaneWithFoodType(){
        privateJetAirplaneBuilder = new PrivateJetAirplaneBuilder();

        final FoodType foodType = new PrecookedFood();
        privateJetAirplaneBuilder.setFoodType(foodType);
        PrivateJetAirplane privateAirplane = privateJetAirplaneBuilder.build();

        assertEquals(foodType, privateAirplane.getFoodType());
    }

    @Test
    public void when_resetIsInvoked_Expect_NewPrivateJetAirplaneInstance(){
        privateJetAirplaneBuilder = new PrivateJetAirplaneBuilder();
        
        final int numberOfWheels = 40;
        privateJetAirplaneBuilder.setNumberOfWheels(numberOfWheels);
        privateJetAirplaneBuilder.reset();
        PrivateJetAirplane privateAirplane = privateJetAirplaneBuilder.build();

        assertNotEquals(numberOfWheels, privateAirplane.getNumberOfWheels());
    }

    @Test
    public void when_buildIsInvoked_Expect_PrivateJetAirplaneWithChampagne(){
        privateJetAirplaneBuilder = new PrivateJetAirplaneBuilder();

        PrivateJetAirplane privateAirplane = privateJetAirplaneBuilder.build();

        assertTrue(privateAirplane.getChampagne() instanceof ChampagneBottle);
    }
}
