**p-generic-clone** is a single class program based on the prototype pattern.

- It completely removes the responsability of deciding which fields a class needs to copy to create a prototype of itself.

- It uses java built-in reflections to access to the classloader and gain control over a class, without the need of marking as public the fields inside a class.

The only requirement for a class to be copied is to have a default constructor defined.