package com.myhouse.company.generic.clone.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GenericInvokerTestUtils {


    public static Object invokeMethod(Object invoker, String methodName) throws Exception {
        try {
            Method method = invoker.getClass().getDeclaredMethod(methodName);
            return method.invoke(invoker);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new Exception("Error invoking method: " + methodName, e);
        }
    }

    public static Object invokeMethod(Object invoker, String methodName, Class<?> parameterTypes, Object... values) throws Exception {
        try {
            Method method = invoker.getClass().getDeclaredMethod(methodName, parameterTypes);
            method.setAccessible(true);
            return method.invoke(invoker, values);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new Exception("Error invoking method: " + methodName, e);
        }
    }

    public static Object getFieldValue(Object invoker, String fieldName) throws Exception {
        try {
            Field field = invoker.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(invoker);
        } catch (IllegalAccessException e) {
            throw new Exception("Error invoking field: " + fieldName, e);
        }
    }
}
