package com.myhouse.company.generic.clone.utils;

import java.util.List;

public class CloneableObject {

    private final String field1 = "field1";
    private final String field2 = "2";
    private final int field3 = 1;
    private final List<String> arrayField = List.of("1", "2");
    private final char noGetterField = '1';
    private final AnotherCloneableObject anotherCloneableObject = new AnotherCloneableObject();
    protected final String protectedField = "abc";


    public String getField1() {
        return field1;
    }

    public String getField2() {
        return field2;
    }

    public int getField3() {
        return field3;
    }

    public List<String> getArrayField() {
        return arrayField;
    }

    public String getProtectedField() {
        return protectedField;
    }

    public AnotherCloneableObject getAnotherCloneableObject() {
        return anotherCloneableObject;
    }
}

class AnotherCloneableObject {
    private final String field1 = "1";
    private final int field2 = 2;
    private final char field3 = 'c';

}
