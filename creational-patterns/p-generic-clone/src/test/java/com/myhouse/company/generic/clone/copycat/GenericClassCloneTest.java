package com.myhouse.company.generic.clone.copycat;

import static org.junit.Assert.*;

import com.myhouse.company.generic.clone.utils.ClassWithoutDefaultConstructor;
import com.myhouse.company.generic.clone.utils.CloneableObject;
import com.myhouse.company.generic.clone.utils.GenericInvokerTestUtils;
import org.junit.Test;

public class GenericClassCloneTest {

    private GenericClassFieldsCopyCat genericClassFieldsCopyCat;


    @Test
    public void when_CloneObjectIsDone_Expect_PrivateFieldsAreCopied() throws Exception {
        genericClassFieldsCopyCat = new GenericClassFieldsCopyCat();

        Object clonedObject = genericClassFieldsCopyCat.cloneObject(new CloneableObject());

        assertEquals("field1", GenericInvokerTestUtils
                .invokeMethod(clonedObject, "getField1"));
        assertEquals("2", GenericInvokerTestUtils
                .invokeMethod(clonedObject, "getField2"));
        assertEquals(1, GenericInvokerTestUtils
                .invokeMethod(clonedObject, "getField3"));
        assertEquals("[1, 2]", GenericInvokerTestUtils
                .invokeMethod(clonedObject, "getArrayField").toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_CloneObjectIsInvokedWithObjectWithoutDefaultConstruct_Expect_Exception() throws Exception {
        genericClassFieldsCopyCat = new GenericClassFieldsCopyCat();

        Object clonedObject = genericClassFieldsCopyCat.cloneObject(new ClassWithoutDefaultConstructor(""));
    }

    @Test
    public void when_CloneObjectIsDone_Expect_ProtectedFieldIsCopied() throws Exception {
        genericClassFieldsCopyCat = new GenericClassFieldsCopyCat();

        Object clonedObject = genericClassFieldsCopyCat.cloneObject(new CloneableObject());

        assertEquals("abc", GenericInvokerTestUtils
                .invokeMethod(clonedObject, "getProtectedField"));
    }

    @Test
    public void when_CloneObjectIsDone_Expect_NoGetterFieldCopied() throws Exception {
        genericClassFieldsCopyCat = new GenericClassFieldsCopyCat();

        Object clonedObject = genericClassFieldsCopyCat.cloneObject(new CloneableObject());

        assertEquals('1', GenericInvokerTestUtils
                .getFieldValue(clonedObject, "noGetterField"));
    }

    @Test
    public void when_CloneObjectIsDone_Expect_DeeperFieldIsCopied() throws Exception {
        genericClassFieldsCopyCat = new GenericClassFieldsCopyCat();
        Object clonedObject = genericClassFieldsCopyCat.cloneObject(new CloneableObject());

        Object deeperObject = GenericInvokerTestUtils
                .invokeMethod(clonedObject, "getAnotherCloneableObject");

        assertEquals("1", GenericInvokerTestUtils.getFieldValue(deeperObject, "field1"));
        assertEquals(2, GenericInvokerTestUtils.getFieldValue(deeperObject, "field2"));
        assertEquals('c', GenericInvokerTestUtils.getFieldValue(deeperObject, "field3"));
    }



}
