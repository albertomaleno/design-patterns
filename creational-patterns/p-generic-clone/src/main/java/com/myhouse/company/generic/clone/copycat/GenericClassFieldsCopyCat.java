package com.myhouse.company.generic.clone.copycat;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * Simple class to copy fields in a generic way as it were the prototype pattern.
 */
public class GenericClassFieldsCopyCat {

    /**
     * Clones every field in an object into a new one
     * @param object to be cloned
     * @return clonedObject
     * @throws IllegalAccessException when no default constructor is present or a field can not be accessed
     */
    public Object cloneObject(Object object) throws IllegalAccessException {
        Object clonedObject = getNewInstance(object);

        Field[] objectFields = object.getClass().getFields();
        for (Field field: objectFields){
            String fieldName = field.getName();
            Object fieldValue = getFieldValue(object, field);
            clonedObject = assignFieldToObject(clonedObject, fieldName, fieldValue);
        }
        return clonedObject;
    }

    private Object getNewInstance(Object object) throws IllegalAccessException {
        Object newInstance = null;
        try {
            newInstance = object.getClass().getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new IllegalArgumentException("Can not create constructor for class: " + object.getClass());
        }
        return newInstance;
    }

    private Object getFieldValue(Object object, Field field){
        field.setAccessible(true);
        Object fieldValue = null;
        try {
            fieldValue = field.get(object);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Field can not be accessed: " + field);
        }
        return fieldValue;
    }

    private Object assignFieldToObject(Object object, String fieldName, Object fieldValue){
        try {
            Field clonedObjectField = object.getClass().getField(fieldName);
            clonedObjectField.setAccessible(true);
            clonedObjectField.set(object, fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new IllegalArgumentException("Can not assign value to cloned field: " + fieldName);
        }
        return object;
    }

}
