package com.myhouse.factory.pattern.factory;

import com.myhouse.factory.pattern.cache.TrainingCache;
import com.myhouse.factory.pattern.model.Athlete;
import com.myhouse.factory.pattern.traning.Training;

public abstract class TodayTraining {

    protected static final TrainingCache TRAINING_CACHE = new TrainingCache();

    public void train(Athlete athlete){
        System.out.println("Athlete " + athlete.getName() +  " starts training");
        Training training = getTraining();
        training.doTrain();
    }

    public abstract Training getTraining();
}
