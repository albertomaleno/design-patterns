package com.myhouse.factory.pattern;

import com.myhouse.factory.pattern.factory.TrainingProvider;
import com.myhouse.factory.pattern.factory.TodayTraining;
import com.myhouse.factory.pattern.model.Athlete;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args){
        TrainingProvider trainingProvider = new TrainingProvider();
        TodayTraining todayTraining = trainingProvider.getTodayTraining(LocalDate.now().getDayOfWeek());
        Athlete athlete = new Athlete();
        athlete.setName("Iker");
        todayTraining.train(athlete);
    }
}
