package com.myhouse.factory.pattern.factory;

import java.time.DayOfWeek;

public class TrainingProvider {

    public TodayTraining getTodayTraining(DayOfWeek dayOfWeek){

        TodayTraining todayTraining;
        if (dayOfWeek.equals(DayOfWeek.MONDAY) || dayOfWeek.equals(DayOfWeek.THURSDAY)){
            todayTraining = new SwimTraining();
        }else if (dayOfWeek.equals(DayOfWeek.TUESDAY) || dayOfWeek.equals(DayOfWeek.FRIDAY)){
            todayTraining = new RunTraining();
        }else {
            todayTraining = new CycleTraining();
            // yuhu! More cycling :)
        }
        return todayTraining;
    }
}
