package com.myhouse.factory.pattern.traning;

public class Running implements Training {

    @Override
    public void doTrain() {
        System.out.println("Running for 30 minutes");
        int start = 0;
        int end = 30;
        while (start < end){
            if (start == 15){
                System.out.println("Been running for 15 minutes, only 15 more left!");
            }
            start++;
        }
        System.out.println("End!");
    }
}
