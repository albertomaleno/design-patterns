package com.myhouse.factory.pattern.factory;

import com.myhouse.factory.pattern.traning.Swimming;
import com.myhouse.factory.pattern.traning.Training;

public class SwimTraining extends TodayTraining {

    @Override
    public Training getTraining() {
        return TRAINING_CACHE.getInstance("Swimming");
    }
}
