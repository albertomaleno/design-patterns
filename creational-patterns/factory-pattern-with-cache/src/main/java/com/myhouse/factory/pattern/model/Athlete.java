package com.myhouse.factory.pattern.model;

public class Athlete {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
