package com.myhouse.factory.pattern.cache;

import com.myhouse.factory.pattern.traning.Running;
import com.myhouse.factory.pattern.traning.Swimming;
import com.myhouse.factory.pattern.traning.Training;
import com.myhouse.factory.pattern.traning.Cycling;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TrainingCache {

    private static final Map<String, Training> INSTANCES_MAP = new ConcurrentHashMap<>();

    static {
        INSTANCES_MAP.put("Swimming", new Swimming());
        INSTANCES_MAP.put("Running", new Running());
        INSTANCES_MAP.put("Cycling", new Cycling());
    }

    public Training getInstance(String trainingClassName){
        return INSTANCES_MAP.get(trainingClassName);
    }

    public void addInstance(Training training){
        String trainingClassName = training.getClass().getName();
        INSTANCES_MAP.put(trainingClassName, training);
    }
}
