package com.myhouse.factory.pattern.factory;

import com.myhouse.factory.pattern.factory.CycleTraining;
import com.myhouse.factory.pattern.factory.RunTraining;
import com.myhouse.factory.pattern.factory.SwimTraining;
import com.myhouse.factory.pattern.factory.TodayTraining;
import com.myhouse.factory.pattern.model.Athlete;
import com.myhouse.factory.pattern.traning.Cycling;
import com.myhouse.factory.pattern.traning.Running;
import com.myhouse.factory.pattern.traning.Swimming;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TodayTrainingTest {

    private TodayTraining todayTraining;

    @Test
    public void when_TrainingIsSwimming_SwimmingTrainingCanBeRetrieved(){
        todayTraining = new SwimTraining();

        assertTrue(todayTraining.getTraining() instanceof Swimming);
    }

    @Test
    public void when_TrainingIsRunning_RunningTrainingCanBeRetrieved(){
        todayTraining = new RunTraining();

        assertTrue(todayTraining.getTraining() instanceof Running);
    }

    @Test
    public void when_TrainingIsCycling_CyclingTrainingCanBeRetrieved(){
        todayTraining = new CycleTraining();

        assertTrue(todayTraining.getTraining() instanceof Cycling);
    }

    @Test
    public void when_TrainingIsSwimming_CanTrain(){
        todayTraining = new SwimTraining();

        todayTraining.train(new Athlete());
    }

    @Test
    public void when_TrainingIsRunning_CanTrain(){
        todayTraining = new RunTraining();

        todayTraining.train(new Athlete());
    }

    @Test
    public void when_TrainingIsCycling_CanTrain(){
        todayTraining = new CycleTraining();

        todayTraining.train(new Athlete());
    }
}
