package com.myhouse.factory.pattern.cache;

import static org.junit.Assert.*;

import com.myhouse.factory.pattern.traning.Cycling;
import com.myhouse.factory.pattern.traning.Running;
import com.myhouse.factory.pattern.traning.Swimming;
import org.junit.Test;

public class TrainingCacheTest {

    @Test
    public void when_SwimmingInstanceIsRequested_CacheReturnsInstance(){
        TrainingCache trainingCache = new TrainingCache();

        assertTrue(trainingCache.getInstance("Swimming") instanceof Swimming);
    }

    @Test
    public void when_RunningInstanceIsRequested_CacheReturnsInstance(){
        TrainingCache trainingCache = new TrainingCache();

        assertTrue(trainingCache.getInstance("Running") instanceof Running);
    }

    @Test
    public void when_CyclingInstanceIsRequested_CacheReturnsInstance(){
        TrainingCache trainingCache = new TrainingCache();

        assertTrue(trainingCache.getInstance("Cycling") instanceof Cycling);
    }

    @Test
    public void when_SwimmingInstanceIsAdded_CacheAddsInstance(){
        TrainingCache trainingCache = new TrainingCache();

        trainingCache.addInstance(new Swimming());
    }
}
