package com.myhouse.factory.pattern.factory;

import static org.junit.Assert.*;

import org.junit.Test;

import java.time.DayOfWeek;

public class TrainingProviderTest {

    private TrainingProvider trainingProvider;

    @Test
    public void when_DayOfWeekIsMondayOrThursday_Expect_SwimTrainingFactory(){
        trainingProvider = new TrainingProvider();
        DayOfWeek monday = DayOfWeek.MONDAY;
        DayOfWeek thursday = DayOfWeek.THURSDAY;

        assertTrue(trainingProvider.getTodayTraining(monday) instanceof SwimTraining);
        assertTrue(trainingProvider.getTodayTraining(thursday) instanceof SwimTraining);
    }

    @Test
    public void when_DayOfWeekIsTuesdayOrFriday_Expect_RunTrainingFactory(){
        trainingProvider = new TrainingProvider();
        DayOfWeek tuesday = DayOfWeek.TUESDAY;
        DayOfWeek friday = DayOfWeek.FRIDAY;

        assertTrue(trainingProvider.getTodayTraining(tuesday) instanceof RunTraining);
        assertTrue(trainingProvider.getTodayTraining(friday) instanceof RunTraining);
    }


    @Test
    public void when_DayOfWeekIsWednesdaySaturdayOrSunday_Expect_CycleTrainingFactory(){
        trainingProvider = new TrainingProvider();
        DayOfWeek wednesday = DayOfWeek.WEDNESDAY;
        DayOfWeek saturday = DayOfWeek.SATURDAY;
        DayOfWeek sunday = DayOfWeek.SUNDAY;

        assertTrue(trainingProvider.getTodayTraining(wednesday) instanceof CycleTraining);
        assertTrue(trainingProvider.getTodayTraining(saturday) instanceof CycleTraining);
        assertTrue(trainingProvider.getTodayTraining(sunday) instanceof CycleTraining);
    }
}
