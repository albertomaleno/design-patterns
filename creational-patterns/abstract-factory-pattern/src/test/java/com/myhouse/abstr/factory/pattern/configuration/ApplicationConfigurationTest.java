package com.myhouse.abstr.factory.pattern.configuration;

import org.junit.Test;


public class ApplicationConfigurationTest {

    private static final String PROPERTIES_FILE_NAME = "sports-configuration.properties";
    private static final String TMP_NAME = "tmp";

    @Test
    public void can_get_factory(){
        ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(new SportsProperties());

        applicationConfiguration.getFactoryConfiguration();
    }

    @Test(expected = ApplicationConfigurationException.class)
    public void application_configuration_error_is_thrown_when_no_properties(){
        ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(null);

        applicationConfiguration.getFactoryConfiguration();
    }

}
