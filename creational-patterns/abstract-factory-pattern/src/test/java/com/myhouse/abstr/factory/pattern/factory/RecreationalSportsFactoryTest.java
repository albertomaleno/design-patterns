package com.myhouse.abstr.factory.pattern.factory;

import static org.junit.Assert.*;

import com.myhouse.abstr.factory.pattern.factory.RecreationalSportsFactory;
import com.myhouse.abstr.factory.pattern.sports.Boules;
import com.myhouse.abstr.factory.pattern.sports.Running;
import com.myhouse.abstr.factory.pattern.sports.SolitarySport;
import com.myhouse.abstr.factory.pattern.sports.TeamSport;
import org.junit.Test;

public class RecreationalSportsFactoryTest {

    @Test
    public void can_create_solitary_sport(){
        RecreationalSportsFactory recreationalSportsFactory = new RecreationalSportsFactory();

        SolitarySport solitarySport = recreationalSportsFactory.createSolitarySport();

        assertTrue(solitarySport instanceof Running);
    }

    @Test
    public void can_create_team_sport(){
        RecreationalSportsFactory recreationalSportsFactory = new RecreationalSportsFactory();

        TeamSport teamSport = recreationalSportsFactory.createTeamSport();

        assertTrue(teamSport instanceof Boules);
    }
}
