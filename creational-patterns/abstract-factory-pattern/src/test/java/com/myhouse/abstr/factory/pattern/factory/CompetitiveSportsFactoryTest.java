package com.myhouse.abstr.factory.pattern.factory;

import static org.junit.Assert.*;

import com.myhouse.abstr.factory.pattern.factory.CompetitiveSportsFactory;
import com.myhouse.abstr.factory.pattern.sports.Football;
import com.myhouse.abstr.factory.pattern.sports.SolitarySport;
import com.myhouse.abstr.factory.pattern.sports.TeamSport;
import com.myhouse.abstr.factory.pattern.sports.Windsurf;
import org.junit.Test;

public class CompetitiveSportsFactoryTest {

    @Test
    public void can_create_solitary_sport(){
        CompetitiveSportsFactory competitiveSportsFactory = new CompetitiveSportsFactory();

        SolitarySport solitarySport = competitiveSportsFactory.createSolitarySport();

        assertTrue(solitarySport instanceof Windsurf);
    }

    @Test
    public void can_create_team_sport(){
        CompetitiveSportsFactory competitiveSportsFactory = new CompetitiveSportsFactory();

        TeamSport teamSport = competitiveSportsFactory.createTeamSport();

        assertTrue(teamSport instanceof Football);
    }
}
