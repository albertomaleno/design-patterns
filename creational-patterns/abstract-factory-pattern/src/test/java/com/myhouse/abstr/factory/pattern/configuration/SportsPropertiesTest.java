package com.myhouse.abstr.factory.pattern.configuration;

import org.junit.Test;

import java.io.IOException;

public class SportsPropertiesTest {

    @Test
    public void can_load_configuration() throws IOException {
        SportsProperties sportsProperties = new SportsProperties();

        sportsProperties.loadConfiguration();
    }

    @Test
    public void can_get_competitive_sport_property() throws IOException {
        SportsProperties sportsProperties = new SportsProperties();

        sportsProperties.loadConfiguration();

        sportsProperties.isCompetitiveSport();
    }
}
