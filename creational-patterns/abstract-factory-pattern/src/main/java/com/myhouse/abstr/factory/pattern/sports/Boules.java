package com.myhouse.abstr.factory.pattern.sports;

public class Boules implements TeamSport {

    @Override
    public int playersCount() {
        return 2;
    }
}
