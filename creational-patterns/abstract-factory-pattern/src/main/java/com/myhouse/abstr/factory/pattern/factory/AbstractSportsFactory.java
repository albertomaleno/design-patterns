package com.myhouse.abstr.factory.pattern.factory;

import com.myhouse.abstr.factory.pattern.sports.SolitarySport;
import com.myhouse.abstr.factory.pattern.sports.TeamSport;

public abstract class AbstractSportsFactory {

    public abstract TeamSport createTeamSport();

    public abstract SolitarySport createSolitarySport();

}
