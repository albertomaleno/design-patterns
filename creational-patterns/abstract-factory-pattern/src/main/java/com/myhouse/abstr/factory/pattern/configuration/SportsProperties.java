package com.myhouse.abstr.factory.pattern.configuration;


import java.io.IOException;
import java.util.Properties;

public class SportsProperties {

    private static final String CONFIGURATION_PROPERTIES_PATH = "sports-configuration.properties";
    private static final String IS_COMPETITIVE_SPORT = "competitive.sport";
    private Properties sportsProperties;


    public void loadConfiguration() throws IOException {
        if (sportsProperties == null){
            loadProperties();
        }
    }

    private void loadProperties() throws IOException {
        sportsProperties = new Properties();
        sportsProperties.load(getClass()
                    .getClassLoader()
                    .getResourceAsStream(CONFIGURATION_PROPERTIES_PATH));
    }

    public boolean isCompetitiveSport(){
        return Boolean.valueOf(sportsProperties.getProperty(IS_COMPETITIVE_SPORT));
    }

}
