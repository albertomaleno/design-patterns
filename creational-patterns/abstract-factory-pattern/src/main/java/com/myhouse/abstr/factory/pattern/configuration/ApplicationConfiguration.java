package com.myhouse.abstr.factory.pattern.configuration;

import com.myhouse.abstr.factory.pattern.factory.AbstractSportsFactory;
import com.myhouse.abstr.factory.pattern.factory.CompetitiveSportsFactory;
import com.myhouse.abstr.factory.pattern.factory.RecreationalSportsFactory;

import java.io.IOException;

public class ApplicationConfiguration {

    private SportsProperties sportsProperties;

    public ApplicationConfiguration(SportsProperties sportsProperties) {
        this.sportsProperties = sportsProperties;
        loadConfiguration(sportsProperties);
    }

    private void loadConfiguration(SportsProperties sportsProperties){
        try{
            sportsProperties.loadConfiguration();
        }catch (IOException | NullPointerException e){
            throw new ApplicationConfigurationException("Can not load properties file", e);
        }
    }

    public AbstractSportsFactory getFactoryConfiguration(){
        try{

            if (sportsProperties.isCompetitiveSport()){
                return new CompetitiveSportsFactory();
            }else{
                return new RecreationalSportsFactory();
            }

        }catch (NullPointerException e){
            throw new ApplicationConfigurationException("Property team sport can not be load", e);
        }
    }
}
