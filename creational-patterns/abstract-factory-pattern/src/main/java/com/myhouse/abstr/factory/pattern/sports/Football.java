package com.myhouse.abstr.factory.pattern.sports;

public class Football implements TeamSport {

    @Override
    public int playersCount() {
        return 11;
    }
}
