package com.myhouse.abstr.factory.pattern;

import com.myhouse.abstr.factory.pattern.configuration.ApplicationConfiguration;
import com.myhouse.abstr.factory.pattern.configuration.SportsProperties;
import com.myhouse.abstr.factory.pattern.factory.AbstractSportsFactory;

public class Main {
    
    public static void main(String[] args){
        ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(new SportsProperties());
        AbstractSportsFactory abstractSportsFactory = applicationConfiguration.getFactoryConfiguration();
        // SomeSportsApplication someSportsApplication = new SomeSportsApplication(abstractSportsFactory);
    }
}
