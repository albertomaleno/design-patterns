package com.myhouse.abstr.factory.pattern.configuration;

public class ApplicationConfigurationException extends RuntimeException {

    public ApplicationConfigurationException(String message) {
        super(message);
    }

    public ApplicationConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
