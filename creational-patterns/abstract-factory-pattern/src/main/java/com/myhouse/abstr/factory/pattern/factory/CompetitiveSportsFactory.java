package com.myhouse.abstr.factory.pattern.factory;

import com.myhouse.abstr.factory.pattern.sports.Football;
import com.myhouse.abstr.factory.pattern.sports.SolitarySport;
import com.myhouse.abstr.factory.pattern.sports.TeamSport;
import com.myhouse.abstr.factory.pattern.sports.Windsurf;

public class CompetitiveSportsFactory extends AbstractSportsFactory {


    @Override
    public TeamSport createTeamSport() {
        return new Football();
    }

    @Override
    public SolitarySport createSolitarySport() {
        return new Windsurf();
    }
}
