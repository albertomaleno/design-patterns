package com.myhouse.abstr.factory.pattern.sports;

public interface SolitarySport {

    /**
     * Returns true if the sport is done at the outside, false if not
     * @return isOutdoor
     */
    boolean isOutdoor();
}
