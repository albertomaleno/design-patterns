package com.myhouse.abstr.factory.pattern.sports;

public interface TeamSport {

    /**
     * Returns the number of players needed for the team
     * @return playerCount
     */
    int playersCount();
}
