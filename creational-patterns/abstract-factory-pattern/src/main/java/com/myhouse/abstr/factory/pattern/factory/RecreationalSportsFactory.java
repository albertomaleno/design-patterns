package com.myhouse.abstr.factory.pattern.factory;

import com.myhouse.abstr.factory.pattern.sports.Boules;
import com.myhouse.abstr.factory.pattern.sports.Running;
import com.myhouse.abstr.factory.pattern.sports.SolitarySport;
import com.myhouse.abstr.factory.pattern.sports.TeamSport;

public class RecreationalSportsFactory extends AbstractSportsFactory {
    @Override
    public TeamSport createTeamSport() {
        return new Boules();
    }

    @Override
    public SolitarySport createSolitarySport() {
        return new Running();
    }
}
